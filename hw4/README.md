# hw4
## Rivision history : Initial

## Table of Contents
- [hw4](#hw4)
  - [Rivision history : Initial](#rivision-history--initial)
  - [Table of Contents](#table-of-contents)
  - [Description](#description)
  - [Content](#content)
  - [Demo](#demo)

***
## Description
  Implement the iterator protocol for the class. Specifically, this involves creating an inner class that overloads the ++, ==, and != operators.


## Content

 For this assignment, you’ll need to write a C++ program. Please write a C++ class “tree_node” with a preorder iterator to supply tree nodes to the loop in following code. You will need to know (or learn) how to use pointers,references, inner classes, and operator overloading in C++.  For the sake of (relative) simplicity, you may assume that the data in a tree node is always an int; this will save you the need to use generics. You may want to use the stack abstraction from the C++ standard library.

```
tree_node* my_tree = ...
...
for(int n: *my_tree){
   cout<<n<<"\n;
}
```
## Demo
 Compile C++ file
 ```
 g++ tree_node.cpp -o tree_node
 ```

 Run the file
 ```
 ./tree_node
 ```

 Output
 ```
 1
 2
 4
 5
 3
 6
 7
 ```