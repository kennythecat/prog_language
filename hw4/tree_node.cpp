#include <iostream>
#include <stack>

class tree_node {
public:
    int data;
    tree_node* left;
    tree_node* right;

    tree_node(int data) : data(data), left(nullptr), right(nullptr) {}

    class iterator {
    private:
        std::stack<tree_node*> stk;

    public:
        iterator(tree_node* root) {
            if (root) stk.push(root);
        }

        bool operator!=(const iterator& other) const {
            return stk.size() != other.stk.size();
        }

        iterator& operator++() {
            if (!stk.empty()) {
                tree_node* node = stk.top();
                stk.pop();
                if (node->right) stk.push(node->right);
                if (node->left) stk.push(node->left);
            }
            return *this;
        }

        int operator*() const {
            return stk.top()->data;
        }
    };

    iterator begin() {
        return iterator(this);
    }

    iterator end() {
        return iterator(nullptr);
    }
};

int main() {
    // Example usage:
    tree_node* my_tree = new tree_node(1);
    my_tree->left = new tree_node(2);
    my_tree->right = new tree_node(3);
    my_tree->left->left = new tree_node(4);
    my_tree->left->right = new tree_node(5);
    my_tree->right->left = new tree_node(6);
    my_tree->right->right = new tree_node(7);

    for (int n : *my_tree) {
        std::cout << n << "\n";
    }

    // Clean up memory
    delete my_tree->right->right;
    delete my_tree->right->left;
    delete my_tree->right;
    delete my_tree->left->right;
    delete my_tree->left->left;
    delete my_tree->left;
    delete my_tree;

    return 0;
}
