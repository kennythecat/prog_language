# hw3

## Rivision history : Initial

## Table of Contents
- [hw3](#hw3)
  - [Rivision history : Initial](#rivision-history--initial)
  - [Table of Contents](#table-of-contents)
  - [Description](#description)
  - [Content](#content)

***
## Description

Answer the following question.

## Content

1. Both interpretation and code generation can be performed by traversal of a syn- tax
tree. Compare these two kinds of traversals. In what ways are they similar/different?
   
   **Answer:**
   
   Both kinds of traversal are most easily written as a set of mutually recursive subroutines, with
   (to first approximation) one routine for each kind of syntax tree node. The code generation
   traversal will tend to visit each node once, to generate corresponding target code. The
   interpretation traversal may never visit certain nodes (if their part of the program isn’t
   needed in this particular execution); other nodes it may visit many times (e.g., if they are part
   of a loop body or a subroutine that is called repeatedly). 
2.  **Give a grammar that captures all levels of precedence for arithmetic expressions in C, as shown in Figure**
   ![expression](./expression.jpg)
   
    **Answer:**
   
    ```
    expression → assignment_expression
    → expression , assignment_expression
    assignment_operator → =|*=|/=|%=|+=|-=|<<=|>>=|&=|^=||=
    assignment_expression → conditional_expression
    → unary_expression assignment_operator assignment_expression
    conditional expression → logical_OR_expression
    → logical_OR_expression ? expression : conditional_expression
    logical OR expression → logical_AND_expression
    → logical_OR_expression || logical_AND_expression
    logical_AND_expression → inclusive_OR_expression
    → logical_AND_expression && inclusive_OR_expression
    inclusive_OR_expression → exclusive_OR_expression
    → inclusive_OR_expression | exclusive_OR_expression
    exclusive_OR_expression → AND_expression
    → exclusive_OR_expression ^ AND_expression
    AND_expression → equality_expression
    → AND_expression & equality_expression
    equality_expression → relational_expression
    → equality_expression == relational_expression
    → equality expression != relational_expression
    relational_expression → shift_expression
    → relational_expression < shift_expression
    → relational_expression > shift_expression
    → relational_expression <= shift_expression
    → relational_expression >= shift_expression
    shift_expression → additive_expression
    → shift_expression << additive_expression
    → shift_expression >> additive_expression
    additive_expression → multiplicative_expression
    → additive_expression + multiplicative_expression
    → additive_expression - multiplicative_expression
    multiplicative_expression → cast expression
    → multiplicative_expression * cast_expression
    → multiplicative_expression / cast_expression
    → multiplicative_expression % cast_expression
    cast_expression → unary_expression
    → ( type name ) cast_expression
    unary_operator → & | * | + | - | ~ | !
    unary_expression → postfix_expression
    → ++ unary_expression
    → -- unary_expression
    → unary_operator cast_expression
    → sizeof unary_expression
    → sizeof ( type_name )
    argument expression list → assignment_expression
    → argument_expression_list , assignment_expression
    argument_expression_list_opt → argument_expression_list
    →
    postfix_expression → primary_expression
    → postfix_expression [ expression ]
    → postfix_expression ( argument_expression_list_opt )
    → postfix_expression . identifier
    → postfix_expression -> identifier
    → postfix_expression ++
    → postfix_expression --
    → ( type_name ) { initializer_list }
    → ( type name ) { initializer list , }
    primary_expression → identifier
    → constant
    → string_literal
    → ( expression )
    ```
    Syntax for initializer list, type name, string literal, constant, and identifier is specified elsewhere in the standard.

3. Consider the following pseudocode
   ```
   x:integer --global

   procedure set_x(n:integer)
    x:=n
   
   procedure print_x()
    write_integer(x)

   procedure first()
    set_x(1)
    print_x()
   
   procedure second()
    x:integer
    set_x(2)
    print_x()

   set_x(0)
   first()
   print_x()
   second()
   print_x()
   ```
   What does this program print if the language uses static scoping? What does it print
with dynamic scoping? Why?

   **Answer:**
  
   **Static Scoping**
  
   With static (or lexical) scoping, the variable bindings are determined at compile time based on their positions within the source code.

   1.  A global variable x is defined.
   2. set_x(0) sets the global x to 0.
   3. first() is called. It calls set_x(1), which sets the global x to 1, and then print_x(), which prints the global x, outputting 1.
   4. print_x() is then called directly, and it prints the global x, which is still 1, outputting 1.
   5. second() is called. It declares a local x, but set_x(2) modifies the global x, setting it to 2, then print_x() prints this global x, outputting 2.
   6. Finally, print_x() is called again, which prints the global x, which is still 2, outputting 2.
  
   With static scoping, the program would print 1 1 2 2.
   
   **Dynamic Scoping**
   
   With dynamic scoping, variable references are resolved by looking up the call stack to find the most recent binding of the variable.

   1. A global variable x is defined.
   2. set_x(0) sets the global x to 0.
   3. first() is called. It sets the global x to 1, and then prints x, outputting 1.
   4. print_x() is then called directly, and it prints the global x, which is still 1, outputting 1.
   5. second() is called. It declares a new local x, then set_x(2) sets this new local x to 2. print_x() prints the most recently defined x, which is now the local x in second(), outputting 2.
   6. Finally, print_x() is called again, and this time it prints the global x (since the scope of second() has ended), which is still 1, outputting 1.
   
   With dynamic scoping, the program would print 1 1 2 1.

   **Summary**
   
   - With static scoping, the program would print 1 1 2 2 because the global x is always being modified and printed.
   - With dynamic scoping, the program would print 1 1 2 1 because set_x() and print_x() operate on the most recently defined x, which changes depending on the currently executing procedure.

4. Consider the following pseudocode
   ```
   x:integer:=1
   y:integer:=2

   procedure add()
    x:=x+y

   procedure second(P:procedure)
    x:integer:=2
    P()
   
   procedure first
    y:integer:=3
    second(add)

   first()
   write_integer(x)
   ```
   1.  What does this program print if the language uses static scoping?
       
       **Answer:**
  
       Under static (or lexical) scoping, the variable bindings are determined by the program structure and remain constant throughout the program execution. Thus, each reference to a variable is resolved by looking up the nesting chain of scopes at the time the code is compiled, not at the time it is run.
          - x and y are global variables with initial values 1 and 2 respectively.
          - first sets a local variable y to 3, but it doesn't affect the global y.
          - second sets a local variable x to 2, but it doesn't affect the global x.
          - add changes the global x using the global y (since static scoping is used).
       
       So, in first, when second(add) is called, add uses the global x and y (which are 1 and 2, not the local ones), and computes x := x + y as x := 1 + 2, resulting in x = 3.
      So, the program prints 3.
   2.  What does it print if the language uses dynamic scoping with deep binding?
       
       **Answer:**
       
       In dynamic scoping with deep binding, the bindings of variables are determined by the environment in which a procedure is passed as an argument. The bindings are "deeply" copied so that the called procedure uses the variable bindings from the time of the procedure parameter's initial association.

       - first sets a local y to 3.
       - second sets a local x to 2.
       - In first, second(add) is called, and add uses the y from first (which is 3) and the x from the global scope (which is 1).
  
       So, x := x + y computes as x := 1 + 3, resulting in x = 4.
       So, the program prints 4.
   3.  What does it print if the language uses dynamic scoping with shallow binding?
       
       **Answer:**
       
       In dynamic scoping with shallow binding, the bindings for free variables are determined by the most recent (shallowest) environment.

       - first sets a local y to 3.
       - second sets a local x to 2.
       - When second(add) is called in first, add uses the most recent x and y, which are 2 and 3.
       
       So, x := x + y computes as x := 2 + 3, resulting in x = 5.

       However, this is a local x in second. The global x remains unchanged and is 1.

       So, the program prints 1.



