# hw1

## Rivision history : Initial

## Table of Contents- [hw1](#hw1)
- [hw1](#hw1)
  - [Rivision history : Initial](#rivision-history--initial)
  - [Table of Contents- hw1](#table-of-contents--hw1)
  - [Description](#description)
  - [Install Ruby](#install-ruby)
  - [How to Run](#how-to-run)
  - [Demo](#demo)

***
## Description
Use Ruby to implement the pipeline from [tf-06.py](./tf-06/tf-06.py)

## Install Ruby

```
sudo apt update
sudo apt install ruby-full
```

## How to Run
```
ruby tf.rb ./tf-06/pride-and-prejudice.txt 
```

## Demo
```
mr - 786
elizabeth - 635
very - 488
darcy - 418
such - 395
mrs - 343
much - 329
more - 327
bennet - 323
bingley - 306
jane - 295
miss - 283
one - 275
know - 239
before - 229
herself - 227
though - 226
well - 224
never - 220
sister - 218
soon - 216
think - 211
now - 209
time - 203
good - 201
```


