# tf-06

## Rivision history : Initial

## Table of Contents
- [tf-06](#tf-06)
  - [Rivision history : Initial](#rivision-history--initial)
  - [Table of Contents](#table-of-contents)
  - [Description](#description)
  - [How to Run](#how-to-run)
  - [Demo](#demo)

***
## Description
Exercises in programming style, 06-pipeline.

Source :
https://github.com/crista/exercises-in-programming-style/blob/master/06-pipeline/tf-06.py

## How to Run
```
python tf-06.py  pride-and-prejudice.txt
```

## Demo
```
mr - 786
elizabeth - 635
very - 488
darcy - 418
such - 395
mrs - 343
much - 329
more - 327
bennet - 323
bingley - 306
jane - 295
miss - 283
one - 275
know - 239
before - 229
herself - 227
though - 226
well - 224
never - 220
sister - 218
soon - 216
think - 211
now - 209
time - 203
good - 201
```


