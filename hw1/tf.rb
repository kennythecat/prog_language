def read_file(_argv)
    """
    Takes a path to a file && returns the entire
    contents of the file as a string
    """
    f = File.open(_argv)
    data = f.read
    
end

def filter_chars_and_normalize(str_data)
    """
    Takes a string and returns a copy with all nonalphanumeric 
    chars replaced by white space
    """
    str_data.gsub(/[^0-9a-zA-Z]/i,' ').downcase
   
 
end

def scan(str_data)
    """
    Takes a string and scans for words, returning
    a list of words.
    """
    str_data.split
end

def remove_stop_words(word_list)
    """ 
    Takes a list of words and returns a copy with all stop 
    words removed 
    """
    f = File.open('./tf-06/stop_words.txt')
    stop_words = f.read.split(",")
    # add single-letter words
    array=Array.new
    i=0
    stop_words.concat(('a'..'z').to_a)
    for w in word_list do
        if not stop_words.include?(w)
            array[i]=w
            i=i+1
        end
    end
array
end

def frequencies(word_list)
    """
    Takes a list of words and returns a dictionary associating
    words with frequencies of occurrence
    """
    word_freqs = {}
    for w in word_list do
        if word_freqs.include?(w) 
            word_freqs[w] += 1
        else
            word_freqs[w] = 1
        end
    end
    return word_freqs
end

def sort(word_freq)
    """
    Takes a dictionary of words and their frequencies
    and returns a list of pairs where the entries are
    sorted by frequency 
    """
    word_freq.sort_by{|el| -el[1] }

end

def print_all(word_freqs)
    """
    Takes a list of pairs where the entries are sorted by frequency and print them recursively.
    """
    
    puts (word_freqs[0][0]).to_s+"-"+(word_freqs[0][1]).to_s
    if not(word_freqs[1...].empty?)
        print_all(word_freqs[1...])
    end
    
    

end
#puts read_file(ARGV[0])
#puts (filter_chars_and_normalize((read_file(ARGV[0]))))
#puts (scan(filter_chars_and_normalize((read_file(ARGV[0])))))
#puts (remove_stop_words(scan(filter_chars_and_normalize((read_file(ARGV[0]))))))
#puts frequencies(remove_stop_words(scan(filter_chars_and_normalize((read_file(ARGV[0]))))))

#puts stop_words.concat(('a'...'z').to_a)



print_all(sort(frequencies(remove_stop_words(scan(filter_chars_and_normalize(read_file(ARGV[0]))))))[0...25])
