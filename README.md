# Programing Language

## Table of Contents
- [Programing Language](#programing-language)
  - [Table of Contents](#table-of-contents)
  - [Guide](#guide)
  - [Description](#description)
  - [Compare OCaml and Ruby](#compare-ocaml-and-ruby)
    - [Use Cases](#use-cases)
    - [Syntax](#syntax)
    - [Type System](#type-system)
    - [Paradigm](#paradigm)
    - [Performance](#performance)
    - [Libraries and Ecosystem](#libraries-and-ecosystem)
    - [Advantages](#advantages)
      - [OCaml](#ocaml)
      - [Ruby](#ruby)
    - [Disadvantages](#disadvantages)
  - [Rivision history : v2.0](#rivision-history--v20)
## Guide
- [hw1](./hw1) - Implement word counting by Ruby
- [hw2](./hw2) - Implement Pipeline by Ocaml
- [hw3](./hw3) - Solve pl problems
- [hw4](./hw4) - Tree Node

***

## Description

This repository contains the following assignments:

- `hw1` (2021/10/14)
  
  Implement parser from python code to Ruby

- `hw2` (2021/10/26)
  
  Using Ocaml to implement parser by pipeline.   

- `hw3` (2021/11/2)
  1. Code Generation
  2. Grammar of arithmetic expressions
  3. Static scoping and Dynamic scoping
  4. Deep binding and Shallow binding

- `hw4` (2021/11/23)
  1. Pointers
  2. References 
  3. Inner classes  
  4. Operator overloading

## Compare OCaml and Ruby
### Use Cases

- **OCaml**
1. **Formal Verification**: Used in mission-critical systems where correctness is paramount.
2. **Scientific Computing**: Suitable for data analysis and numerical simulations.
3. **Compiler Development**: Often used in the development of compilers and interpreters.

- **Ruby**
1. **Web Development**: Ruby on Rails is a popular framework for building web applications.
2. **Scripting and Automation**: Excellent for writing scripts for various tasks.
3. **Data Analysis**: With libraries like RubyNum and Daru, it's gaining traction in data analysis.


### Syntax
- **OCaml**: Functional and statically-typed language with a more formal syntax.
- **Ruby**: Object-oriented and dynamically-typed language with a more readable and expressive syntax.

### Type System
- **OCaml**: Strong, static type system with type inference.
- **Ruby**: Dynamic type system.

### Paradigm
- **OCaml**: Primarily functional but supports object-oriented and imperative programming.
- **Ruby**: Primarily object-oriented but supports functional and imperative programming.

### Performance
- **OCaml**: Generally faster due to static typing and native compilation.
- **Ruby**: Slower due to dynamic typing and interpreted nature.

### Libraries and Ecosystem
- **OCaml**: Rich ecosystem for scientific computing and formal verification but less extensive for web development.
- **Ruby**: Extensive libraries and frameworks, especially for web development (e.g., Ruby on Rails).

---

### Advantages

#### OCaml
1. **Performance**: Faster execution due to static typing and native compilation.
2. **Type Safety**: Strong type system catches errors at compile-time.
3. **Functional Programming**: Excellent support for functional programming paradigms.

#### Ruby
1. **Readability**: Human-friendly syntax makes it easy to read and write code.
2. **Rapid Development**: Dynamic typing and extensive libraries enable quick prototyping.
3. **Community**: Large, active community and a wealth of open-source libraries.

---

### Disadvantages

**OCaml**
1. **Learning Curve**: Takes time to get used to the functional paradigm and type system.
2. **Library Support**: Less extensive libraries for certain applications like web development.
3. **Community Size**: Smaller community means fewer resources and support.

**Ruby**
1. **Performance**: Slower execution speed compared to statically-typed languages.
2. **Type Errors**: Dynamic typing means type errors can occur at runtime.
3. **Memory Consumption**: Generally consumes more memory.

---


## Rivision history : v2.0
