# hw2
## Rivision history : Initial

## Table of Contents
- [hw2](#hw2)
  - [Rivision history : Initial](#rivision-history--initial)
  - [Table of Contents](#table-of-contents)
  - [Description](#description)
  - [Setup OCaml](#setup-ocaml)
  - [How to Run](#how-to-run)
  - [Demo](#demo)

***
## Description
Use OCaml to implement the pipeline from [tf-06.py](../hw1/tf-06/tf-06.py)

## Setup OCaml

1. Update Package Lists and Install Required Packages
    ``` 
    sudo apt update
    sudo apt install build-essential m4 -y
    ```

2. Install OPAM
   ```
    sudo apt install opam
    opam init
   ```
   
3. Install OCaml & Library & OUnit2
   ```
    opam switch create 4.12.0  # Replace 4.12.0 with the version you want
    opam install ocaml
    opam install core
    opam install ounit2
    eval $(opam env)
    eval $(opam env --switch=default)
   ```

4. Check OCaml Installation
   ```
   ocaml -version
   ```

## How to Run
1.  Compile the [wf_hash_main.ml](./wf_hash_main.ml) file
    ```
    ocamlfind ocamlopt -c -package core,str -o WF_hash_main.cmx WF_hash_main.ml

    ```

2.  Compile the [wf_hash_test.ml](./wf_hash_test.ml) file
    ```
    ocamlfind ocamlopt -c -package core,str,ounit2 -o wf_hash_test.cmx wf_hash_test.ml
    ```

3.  Link them together to create an executable file
    ```
    ocamlfind ocamlopt -o wf_hash_test -package core,str,ounit2 -linkpkg WF_hash_main.cmx wf_hash_test.cmx
    ```

4.  Link them together to create an executable file
    ```
    ./wf_hash_test
    ```

## Demo
```
mr - 786
elizabeth - 635
very - 488
darcy - 418
such - 395
mrs - 343
much - 329
more - 327
bennet - 323
bingley - 306
jane - 295
miss - 283
one - 275
know - 239
before - 229
herself - 227
though - 226
well - 224
never - 220
sister - 218
soon - 216
think - 211
now - 209
time - 203
good - 201
```


