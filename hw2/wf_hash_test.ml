open OUnit2
open WF_hash_main

let htb = Hashtbl.create 1024;;

let l1=[];;
Hashtbl.add htb "a" 2 ;;

let test_take="Test_take">:::
[
    "Case fail" >::(fun _ -> assert_equal [] ( try  (take 6 []) with failwith -> []));
    "Case fail" >::(fun _ -> assert_equal [1] (take 1 [1;2]));
]

let test_touch="Test touch">:::
[
    "Case None" >::(fun _ -> assert_equal (Some 1) (Hashtbl.find_opt (touch htb "c") "c"));
    "Case Some" >::(fun _ -> assert_equal (Some 3) (Hashtbl.find_opt (touch htb "a") "a"));
]

let test_is_in="Test is_in">:::
[
    "Case in    " >::(fun _ -> assert_equal true  (is_in "a" ["a";"b"]));
    "Case not in" >::(fun _ -> assert_equal false (is_in "a" ["c";"b"]));
]

let test_word_count="Test word_count">:::
[
    "Case Empty"  >::(fun _ -> assert_equal [("a",2)] (word_count htb [] []));
    "Case in   "  >::(fun _ -> assert_equal [("a",2)] (word_count htb ["a"] ["a"]));
    "Case not int">::(fun _ -> assert_equal [("a",3)] (word_count htb ["a"] ["b"]));
]


   
    


let _=run_test_tt_main test_touch
let _=run_test_tt_main test_is_in
let _=run_test_tt_main test_word_count
let _=run_test_tt_main test_take


