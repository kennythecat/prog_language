open Stdio
exception Error
let is_in w wl = 
    List.exists (fun word -> word = w) wl;; (*if w in wl return true*)


(* option *)
let touch words word =      
    match Hashtbl.find_opt words word with
    | None -> begin Hashtbl.add words word 1 ;words end
    | Some x ->  begin Hashtbl.replace words word (succ x); words end
  
  (*(List.remove_assoc word words)*) (*count repeat times of each single,並把計算過的從List中剃除*)
(*
let touch words word =   
  let count = 
    match Hashtbl.find_opt word_freqs word words with(*word是雜湊表*)
    | None -> 0
    | Some x -> x
  in
  insert word (count+1) (List.remove_assoc word words)
  *)
let rec word_count word_freqs words stop_words = (*word_count(list, 被切割好的文字, 切割好的stop_word)*) (*要改*)
    match words with                     
    | [] -> Hashtbl.fold (fun word count acc -> (word, count)::acc) word_freqs [] (*確認list是否empty*)
    | w::ws -> if (is_in w stop_words) 
               then word_count word_freqs ws stop_words(*If w in stop_words 繼續遞迴*)
               else word_count (touch word_freqs w) ws stop_words(**) 

let rec take k xs = match xs with 
    | [] -> raise Error
    | x::xs -> if k=1 then [x] else x::take (k-1) xs;;
let fgeneric () = failwith "Not implemented";;

let run() =
    let stop_words = 
        In_channel.read_all "./stop_words.txt"
        |> Str.split (Str.regexp "[,]")     (*回傳以逗點區隔*)
    in 
    let text = 
        In_channel.read_all "./pride-and-prejudice.txt"
        |> Str.global_replace  (Str.regexp "[^a-zA-Z0-9]+") " " (*把非一般文字換成空格*)
        |> String.lowercase_ascii
        |> Str.split (Str.regexp "[ ]")           (*以空格區分*)
    in 
  
  word_count (Hashtbl.create 90) text stop_words         
  |> List.sort (fun (_,x) (_,y) -> compare y x) 
  |> take 20
  |> List.iter (fun (word,count) -> printf "%3d: %s\n" count word);;

run()
